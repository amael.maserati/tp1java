public class Personnage {
    /** Le nom du personnage */
    private String nom;
    /** Cet attribut modélise la taille de la barbe du personnage */
    private int tailleBarbe;
    /** Cet attribut modélise  la taille des oreilles du personnage*/
    private int tailleOreilles;
    /** Permet de créer un personnage 
     * @param nom une chaîne de caractères définissant le nom du personnage
     * @param barbe un nombre entier définissant la taille de la barbe du personnage
     * @param oreille un nombre entier définissant la taille des oreilles du personnage
    */
    public Personnage(String nom, int barbe, int oreille) {
        this.nom = nom;
        this.tailleBarbe = barbe;
        this.tailleOreilles = oreille;
    }
    /** Cette méthode permet d'obtenir le nom du personnage */
    public String getNom() {
        return this.nom; 
    }
    /** Cette méthode permet d'obtenir la taille de la barbe du personnage */
    public int getBarbe() {
        return this.tailleBarbe;
    }
    /** Cette méthode permet d'obtenir la taille des oreilles du personnage */
    public int getTailleOreilles() {
        return this.tailleOreilles;
    }
    @Override
    /** Cette méthode permet d'écrire les caractéristiques du personnage */
    public String toString() {
        return "(" + "nom : " + this.nom + "," + " taille de la Barbe : " + this.tailleBarbe + "," + " taille des oreilles : " + this.tailleOreilles + ")";
    }
}