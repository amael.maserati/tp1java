public class ExecutablePersonnage {
    public static void main(String [] args) {
        Personnage nain = new Personnage("Gimli", 65, 15);
        System.out.println(nain.getNom());
        System.out.println(nain.getTailleOreilles());
        System.out.println(nain.getBarbe());
        System.out.println(nain.toString());
        // Tests pour les méthodes getNom(), getBarbe() et getTailleOreilles()
        Personnage exemple = new Personnage("Lenain", 45, 10);
        assert exemple.getNom() == "Lenain";
        assert exemple.getBarbe() == 45;
        assert exemple.getTailleOreilles() == 10;
    }
}
