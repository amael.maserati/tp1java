public class ExecutablePokemon {
    public static void main(String [] args) {
        Pokemon poke = null;
        poke = new Pokemon ("Bulbizarre" , 30);
        poke.evolue("Herbizarre" , 37);
        poke.evolue ("Florizarre");
        poke = new Pokemon ("Abo", 10);
        poke.evolue("Arbok", 24);

        System.out.println(poke.toString()); // (1) Cela affiche la dernière évolution du pokemon créé avec sa force finale
    }
}